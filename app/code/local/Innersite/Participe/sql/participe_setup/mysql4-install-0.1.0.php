<?php
 
	$installer = $this;
 
	$installer->startSetup();
 
	$installer->run("

		CREATE TABLE IF NOT EXISTS {$this->getTable('participe')} (
  		`participe_id` int(11) unsigned NOT NULL auto_increment,
		`titulo_promo` text,
        `desc_promo` text,
        `data_vigencia` datetime default NULL,
        `tipo_promo` int NOT NULL,
        `tipo_votacao` int NOT NULL,
        `regulamento_promo` text,
        `video_promo` text,
        `bg_promo` text,
        `img_promo` text,
        `status` varchar(255) DEFAULT 0,
        `created_time` datetime NULL,
        `update_time` datetime NULL,
  			PRIMARY KEY (`participe_id`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;

    CREATE TABLE IF NOT EXISTS `participacao` (
        `participacao_id` int(11) unsigned NOT NULL auto_increment,
        `nome_usuario` varchar(255),
        `email_usuario` varchar(255),
        `tipo_promocao` int NOT NULL,
        `url_video` text,
        `foto_participacao_promo` text,
        `mensagem` text,
        `data_votacao` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
        `finalista` tinyint DEFAULT 0,
        `vencedor` tinyint DEFAULT 0,
        `id_promocao` int(11) NOT NULL,
        PRIMARY KEY (`participacao_id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

    CREATE TABLE IF NOT EXISTS `votacao` (
        `votacao_id` int(11) unsigned NOT NULL auto_increment,
        `id_participacao` varchar(255),
        `id_fb_votador` varchar(255),
        `email_votador` varchar(255),
        `data_votacao` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY (`votacao_id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
  ");
	$installer->endSetup();
