<?php
 
class Innersite_Participe_Adminhtml_ParticipeController extends Mage_Adminhtml_Controller_Action
{
 
    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('participe/items')
            ->_addBreadcrumb(Mage::helper('adminhtml')->__('Items Manager'), Mage::helper('adminhtml')->__('Item Manager'));
        return $this;
    }   
   
    public function indexAction() {
        $this->_initAction();       
        $this->_addContent($this->getLayout()->createBlock('participe/adminhtml_participe'));
        $this->renderLayout();
    }
 
    public function editAction()
    {
        $participeId     = $this->getRequest()->getParam('id');
        $participeModel  = Mage::getModel('participe/participe')->load($participeId);
 
        if ($participeModel->getId() || $participeId == 0) {
 
            Mage::register('participe_data', $participeModel);
 
            $this->loadLayout();
            $this->_setActiveMenu('participe/items');
           
            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item Manager'), Mage::helper('adminhtml')->__('Item Manager'));
            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item News'), Mage::helper('adminhtml')->__('Item News'));
           
            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
           
            $this->_addContent($this->getLayout()->createBlock('participe/adminhtml_participe_edit'))
                 ->_addLeft($this->getLayout()->createBlock('participe/adminhtml_participe_edit_tabs'));
               
            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('participe')->__('Item does not exist'));
            $this->_redirect('*/*/');
        }
    }
   
    public function participacao_editAction()
    {
        $participacaoId     = $this->getRequest()->getParam('id');
        $participacao  = Mage::getModel('participe/participacao')->load($participacaoId);
 
        if ($participacao->getId() || $participacaoId == 0) {
 
            Mage::register('participacao_data', $participacao);
 
            $this->loadLayout();
            $this->_setActiveMenu('participe/items');
           
            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item Manager'), Mage::helper('adminhtml')->__('Item Manager'));
            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item News'), Mage::helper('adminhtml')->__('Item News'));
           
            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
           
            $this->_addContent($this->getLayout()->createBlock('participe/adminhtml_participe_edit_formparticipacao'));
               
            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('participe')->__('Item does not exist'));
            $this->_redirect('*/*/');
        }
    }
   

    public function newAction()
    {
        $this->_forward('edit');
    }
   
    public function saveAction()
    {   
        if ( $this->getRequest()->getPost() ) {
            try {
                $postData = $this->getRequest()->getPost();
                $participeModel = Mage::getModel('participe/participe');
                $participeModel->setId($this->getRequest()->getParam('id'))
                    ->setTitulo_promo($postData['titulo_promo'])
                    ->setDesc_promo($postData['desc_promo'])
                    ->setData_vigencia($postData['data_vigencia'])
                    ->setTipo_promo($postData['tipo_promo'])
                    ->setTipo_votacao($postData['tipo_votacao'])
                    ->setVideo_promo($postData['video_promo'])
                    ->setBg_promo($postData['bg_promo_value'])
                    ->setImg_promo($postData['img_promo_value'])
                    ->setRegulamento_promo($postData['regulamento_promo'])
                    ->setStatus($postData['status_promo'])
                    ->save();
                
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Item was successfully saved'));
                Mage::getSingleton('adminhtml/session')->setParticipeData(false);
                
                if(isset($_FILES) and (file_exists($_FILES['bg_promo']['tmp_name']) or file_exists($_FILES['img_promo']['tmp_name']))) {
                    foreach ($_FILES as $key => $foto) {
                        try {
                            $uploader = new Varien_File_Uploader($key);
                            $uploader->setAllowedExtensions(array('jpg','jpeg','gif','png')); // or pdf or anything
                         
                            // move your file in a folder the magento way                 
                            $uploader->setAllowRenameFiles(false);
                            $uploader->setFilesDispersion(false);

                            $path = Mage::getBaseDir('media') . DS . 'participe' . DS ;

                            $uploader->save($path, $foto['name']);

                            $data[$key] = $foto['name'];
                          }catch(Exception $e) {
                         
                          }
                    }
                }
                $this->_redirect('*/*/');
                return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setParticipeData($this->getRequest()->getPost());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        $this->_redirect('*/*/');
    }
    public function savestatusparticipanteAction(){
        if ( $this->getRequest()->getPost() ) {
            try {
                $postData = $this->getRequest()->getPost();

                $participacaoModel = Mage::getModel('participe/participacao');

                $participacaoModel->setId($this->getRequest()->getParam('id'))
                ->setFinalista($postData['finalista'])
                ->setVencedor($postData['vencedor'])
                ->save();
                
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Item was successfully saved'));
                Mage::getSingleton('adminhtml/session')->setParticipeData(false);
            } catch (Exception $e) {
                Mage::log($e->getMessage());
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setParticipeData($this->getRequest()->getPost());
                $this->_redirect('*/*/participacao_edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        $this->_redirect('*/*/');
    }

    public function deleteAction()
    {
        if( $this->getRequest()->getParam('id') > 0 ) {
            try {
                $participeModel = Mage::getModel('participe/participe');
               
                $participeModel->setId($this->getRequest()->getParam('id'))
                    ->delete();
                   
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Item was successfully deleted'));
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            }
        }
        $this->_redirect('*/*/');
    }
    /**
     * Product grid for AJAX request.
     * Sort and filter result for example.
     */
    public function gridAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody(
               $this->getLayout()->createBlock('participe/adminhtml_participe_grid')->toHtml()
        );
    }
}
