<?php
	class Innersite_Participe_IndexController extends Mage_Core_Controller_Front_Action{

        public function indexAction(){
           	 	$this->loadLayout();
            		$this->renderLayout();
    		}
    		public function singleAction(){
              $usuario = Mage::getModel('customer/customer')->load('1');
              echo "<pre>".print_r($usuario)."</pre>";

           	 	$getParam = Mage::app()->getRequest()->getParam('id');

              $promo = Mage::getModel('participe/participe')->load($getParam);
              $this->loadLayout();
              if($promo->getStatus() == 1){
                $block = $this->getLayout()->getBlock('participe_single');
                $block->setPromo($promo);
              }elseif ($promo->getStatus() == 2) {
                $block = $this->getLayout()->getBlock('participe_single');
                $block->setPromo($promo);
                
                $collection = Mage::getModel('participe/participacao')->getCollection()->addFieldToFilter('id_promocao', $getParam);
                $participantes = $collection->getData();



                $infoParticipante = [];
                foreach ($participantes as $participante) {
                  $participacao_id = $participante['participacao_id'];
                  

                  try {
                    $countThis = $this->getCountVotes($participacao_id);
                    $infoParticipante[$participacao_id] = $countThis;
                  } catch (Exception $e) {

                  }
                }







                $block->setInfoVoteParticipante($infoParticipante);
                $block->setFinalistas($participantes);
              }
              elseif ($promo->getStatus() == 3) {
                $collection = Mage::getModel('participe/participacao')->getCollection()->addFieldToFilter('vencedor', 1);
                $participantes = $collection->getData();
                $block = $this->getLayout()->getBlock('participe_single_vencedor');
              }
           	 	
            	$this->renderLayout();
    		}
        public function participacaoAction(){
            $postData = Mage::app()->getRequest()->getPost();
            $participacaoModel = Mage::getModel('participe/participacao');
            $participacaoModel->setId($this->getRequest()->getParam('id'))
              ->setNome_usuario($postData['nome_usuario'])
              ->setEmail_usuario($postData['email_usuario'])
              ->setTipo_promocao($postData['tipo_promocao'])
              ->setUrl_video($postData['url_video'])
              ->setFoto_participacao_promo($_FILES['foto_participacao_promo']['name'])
              ->setId_promocao($postData['id_promocao'])
              ->setMensagem($postData['message'])
              ->save();


              if(isset($_FILES['foto_participacao_promo']['name']) and (file_exists($_FILES['foto_participacao_promo']['tmp_name']))) {
                try {
                  $uploader = new Varien_File_Uploader('foto_participacao_promo');
                  $uploader->setAllowedExtensions(array('jpg','jpeg','gif','png'));
               
               
                  $uploader->setAllowRenameFiles(false);
                  $uploader->setFilesDispersion(false);
                 
                  $folderByEmail = $postData['email_usuario'];

                  $path = Mage::getBaseDir('media') . DS . 'participe' . DS . 'participacao' . DS;
                             
                  $uploader->save($path, $_FILES['foto_participacao_promo']['name']);
               
                  $data['foto_participacao_promo'] = $_FILES['foto_participacao_promo']['name'];
                }catch(Exception $e) {
               
                }
              }


              $this->_redirect('*/*/sucesso');
        }

        public function votoAction(){
          $postData = Mage::app()->getRequest()->getPost();
          $votacaoModel = Mage::getModel('participe/votacao');
          
          $data = array(
            'status' => false,
            'qtdVotos'=> 0
          );

          $votacaoModel->setId($this->getRequest()->getParam('id'))
            ->setId_participacao($postData['id_participacao'])
            ->setId_fb_votador($postData['id_fb_votador'])
            ->setEmail_votador($postData['email_votador'])
            ->setPromocao_id($postData['id_promocao'])
            ->save();

          if($votacaoModel->isObjectNew()){
            $count = $this->getCountVotes($postData['id_participacao']);

            $data['status'] = true;
            $data['qtdVotos'] = $count;
          };

          echo json_encode($data);
        }

        private function getCountVotes($id_participacao){
            $votacaoModel = Mage::getModel('participe/votacao');
            $votacaoCount = $votacaoModel->getCollection()->addFieldToFilter('id_participacao', $id_participacao)->count();

            if($votacaoCount <= 0){
              Mage::throwException(__METHOD__."Exception - Id inesistente");
            }
            
            return $votacaoCount;
        }

        public function sucessoAction(){
          $this->loadLayout();
          $this->getLayout()->getBlock('participe_sucesso');
          $this->renderLayout();
        }
	}
