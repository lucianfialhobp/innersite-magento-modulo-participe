<?php
 
class Innersite_Participe_Block_Adminhtml_Participe_Grid_Participacao extends Mage_Adminhtml_Block_Widget_Grid{
public function __construct()
    {
        parent::__construct();
        $this->setId('participeGrid');
        // This is the primary key of the database
        $this->setDefaultSort('participacao_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }
 
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('participe/participacao')->getCollection()
                      ->addFieldToFilter('id_promocao', $this->getRequest()->getParam('id'));
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }
 
    protected function _prepareColumns()
    {
        $this->addColumn('participacao_id', array(
            'header'    => Mage::helper('participe')->__('ID'),
            'align'     =>'right',
            'width'     => '50px',
            'index'     => 'participacao_id',
        ));
        
        $this->addColumn('nome_usuario', array(
            'header'    => Mage::helper('participe')->__('Nome Usuário'),
            'align'     =>'left',
            'index'     => 'nome_usuario',
        ));

        $this->addColumn('email_usuario', array(
            'header'    => Mage::helper('participe')->__('Email usuário'),
            'align'     =>'left',
            'index'     => 'email_usuario',
        ));
        
        $this->addColumn('tipo_promocao', array(
            'header'    => Mage::helper('participe')->__('Tipo promocao'),
            'align'     =>'left',
            'width'     => '50px',
            'index'     => 'tipo_promocao',
        ));
        
        $this->addColumn('url_video', array(
            'header'    => Mage::helper('participe')->__('Url do video'),
            'width'     => '150px',
            'index'     => 'url_video',
        ));
        
        $this->addColumn('foto_participacao_promo', array(
            'header'    => Mage::helper('participe')->__('Foto participacao'),
            'width'     => '150px',
            'index'     => 'foto_participacao_promo',
        ));

        $this->addColumn('mensagem', array(
            'header'    => Mage::helper('participe')->__('Mensagem do participante'),
            'width'     => '200px',
            'index'     => 'mensagem',
        ));

 		$this->addColumn('data_votacao', array(
            'header'    => Mage::helper('participe')->__('Data do voto'),
            'width'     => '150px',
            'index'     => 'data_votacao',
        ));

        // $this->addColumn('created_time', array(
        //     'header'    => Mage::helper('participe')->__('Creation Time'),
        //     'align'     => 'left',
        //     'width'     => '120px',
        //     'type'      => 'date',
        //     'default'   => '--',
        //     'index'     => 'created_time',
        // ));
 
        // $this->addColumn('update_time', array(
        //     'header'    => Mage::helper('participe')->__('Update Time'),
        //     'align'     => 'left',
        //     'width'     => '120px',
        //     'type'      => 'date',
        //     'default'   => '--',
        //     'index'     => 'update_time',
        // ));   
 
        return parent::_prepareColumns();
    }
 
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/participacao_edit', array('id' => $row->getId()));
    }
 
    public function getGridUrl()
    {
      return $this->getUrl('*/*/grid', array('_current'=>true));
    }
 
 
}
