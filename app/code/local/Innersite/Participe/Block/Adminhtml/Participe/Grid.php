<?php
 
class Innersite_Participe_Block_Adminhtml_Participe_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('participeGrid');
        // This is the primary key of the database
        $this->setDefaultSort('participe_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }
 
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('participe/participe')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }
 
    protected function _prepareColumns()
    {
        $this->addColumn('participe_id', array(
            'header'    => Mage::helper('participe')->__('ID'),
            'align'     =>'right',
            'width'     => '50px',
            'index'     => 'participe_id',
        ));
        
        $this->addColumn('titulo_promo', array(
            'header'    => Mage::helper('participe')->__('Title promotion'),
            'align'     =>'left',
            'index'     => 'titulo_promo',
        ));

        $this->addColumn('desc_promo', array(
            'header'    => Mage::helper('participe')->__('Description'),
            'align'     =>'left',
            'index'     => 'desc_promo',
        ));
        
        $this->addColumn('data_vigencia', array(
            'header'    => Mage::helper('participe')->__('Title'),
            'align'     =>'left',
            'index'     => 'data_vigencia',
        ));

        
        $this->addColumn('tipo_promo', array(
            'header'    => Mage::helper('participe')->__('Type of promotion'),
            'width'     => '150px',
            'index'     => 'tipo_promo',
        ));
        
        $this->addColumn('tipo_votacao', array(
            'header'    => Mage::helper('participe')->__('Type of vote'),
            'width'     => '150px',
            'index'     => 'tipo_votacao',
        ));
 
        // $this->addColumn('created_time', array(
        //     'header'    => Mage::helper('participe')->__('Creation Time'),
        //     'align'     => 'left',
        //     'width'     => '120px',
        //     'type'      => 'date',
        //     'default'   => '--',
        //     'index'     => 'created_time',
        // ));
 
        // $this->addColumn('update_time', array(
        //     'header'    => Mage::helper('participe')->__('Update Time'),
        //     'align'     => 'left',
        //     'width'     => '120px',
        //     'type'      => 'date',
        //     'default'   => '--',
        //     'index'     => 'update_time',
        // ));   

 
        return parent::_prepareColumns();
    }
 
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }
 
    public function getGridUrl()
    {
      return $this->getUrl('*/*/grid', array('_current'=>true));
    }
 
 
}
