<?php
 
class Innersite_Participe_Block_Adminhtml_Participe_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
 
    public function __construct()
    {
        parent::__construct();
        $this->setId('participe_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('participe')->__('Área de promoções bemglo'));
    }
 
    protected function _beforeToHtml()
    {
        $this->addTab('form_section', array(
            'label'     => Mage::helper('participe')->__('Cadastre uma promoção'),
            'title'     => Mage::helper('participe')->__('Cadastre uma promoção'),
            'content'   => $this->getLayout()->createBlock('participe/adminhtml_participe_edit_tab_form')->toHtml(),
        ));
        $this->addTab('participacao', array(
            'label'     => Mage::helper('participe')->__('Participantes dessa promoção'),
            'title'     => Mage::helper('participe')->__('Participantes dessa promoção'),
            'content'   => $this->getLayout()->createBlock('participe/adminhtml_participe_grid_participacao')->toHtml(),
        ));

        return parent::_beforeToHtml();
    }
}
