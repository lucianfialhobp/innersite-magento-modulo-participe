<?php
 
class Innersite_Participe_Block_Adminhtml_Participe_Edit_FormParticipacao extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form(array(
                                        'id' => 'participacao_form',
                                        'action' => $this->getUrl('*/*/savestatusparticipante', array('id' => $this->getRequest()->getParam('id'))),
                                        'method' => 'post',
                                        'enctype' => 'multipart/form-data',
                                     )
        );
 
        $form->setUseContainer(true);
        $this->setForm($form);


        $participante_id = Mage::app()->getRequest()->getParam('id');
        $participante = Mage::getModel('participe/participacao')->load($participante_id);

        $fieldset = $form->addFieldset('participacao_form', array('legend'=>Mage::helper('participe')->__('Participante information')));

        $fieldset->addField('finalista', 'radios', array(
          'label'     => Mage::helper('participe')->__('Finalista'),
          'name'      => 'finalista',
          'value'  => $participante->getFinalista(),
          'values' => array(
                            array('value'=>1,'label'=>'Sim'),
                            array('value'=>0,'label'=>'Não'),
                       ),
          'disabled' => false,
          'readonly' => false,
          'tabindex' => 1
        ));

        $fieldset->addField('vencedor', 'radios', array(
          'label'     => Mage::helper('participe')->__('Vencedor'),
          'name'      => 'vencedor',
          'value'  => $participante->getVencedor(),
          'values' => array(
                            array('value'=>'1','label'=>'Sim'),
                            array('value'=>'0','label'=>'Não'),
                       ),
          'disabled' => false,
          'readonly' => false,
          'tabindex' => 1
        ));

        $fieldset->addField('submit', 'submit', array(
          'label'     => Mage::helper('participe')->__('Salvar alteração'),
          'required'  => true,
          'value'  => 'Salvar alteração',
          'tabindex' => 1
        ));

        if ( Mage::getSingleton('adminhtml/session')->getParticipeData() ){   
            $form->setValues(Mage::getSingleton('adminhtml/session')->getParticipeData());
            Mage::getSingleton('adminhtml/session')->setParticipeData(null);
        } elseif ( Mage::registry('participe_data') ) {
            $form->setValues(Mage::registry('participe_data')->getData());
        }
        return parent::_prepareForm();

    }
}
