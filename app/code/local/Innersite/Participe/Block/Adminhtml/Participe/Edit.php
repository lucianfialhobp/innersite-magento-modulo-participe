<?php
 
class Innersite_Participe_Block_Adminhtml_Participe_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
               
        $this->_objectId = 'id';
        $this->_blockGroup = 'participe';
        $this->_controller = 'adminhtml_participe';
 
        $this->_updateButton('save', 'label', Mage::helper('participe')->__('Salvar promoção'));
        $this->_updateButton('delete', 'label', Mage::helper('participe')->__('Excluir promoção'));
    }
 
    public function getHeaderText()
    {
        if( Mage::registry('participe_data') && Mage::registry('participe_data')->getId() ) {
            return Mage::helper('participe')->__("Edit Item '%s'", $this->htmlEscape(Mage::registry('participe_data')->getTitle()));
        } else {
            return Mage::helper('participe')->__('Adicionar promoção');
        }
    }
}
