<?php
 
class Innersite_Participe_Block_Adminhtml_Participe_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form{
    protected function _prepareForm(){
        $getParam = Mage::app()->getRequest()->getParam('id');
        $model = Mage::getModel('participe/participe')->load($getParam);
        $bg_promo = $model->getBg_promo();
        $img_promo = $model->getImg_promo();
        $status = $model->getStatus();

        $path = Mage::getBaseUrl('media') . DS . 'participe' . DS ;

        $form = new Varien_Data_Form();
        $this->setForm($form);

        $fieldset = $form->addFieldset('participe_form', array('legend'=>Mage::helper('participe')->__('Item information')));

        
        $fieldset->addField('titulo_promo', 'text', array(
            'label'     => Mage::helper('participe')->__('Title promotion'),
            'class'     => 'required-entry',
            'required'  => true,
            'name'      => 'titulo_promo',
        ));

        $fieldset->addField('desc_promo', 'editor', array(
            'name'      => 'desc_promo',
            'label'     => Mage::helper('participe')->__('Description promotion'),
            'title'     => Mage::helper('participe')->__('Description promotion'),
            'style'     => 'width:98%; height:150px;',
            'wysiwyg'   => false,
            'required'  => true,
        ));
        
        $fieldset->addField('data_vigencia', 'date', array(
            'name'               => 'data_vigencia',
            'label'              => Mage::helper('participe')->__('Date'),
            'after_element_html' => '<small>Selecione o ícone ao lado para selecionar a data</small>',
            'tabindex'           => 1,
            'image'              => $this->getSkinUrl('images/grid-cal.gif'),
            'format'             => 'yyyy-MM-dd',
            'value'              => date( Mage::app()->getLocale()->getDateStrFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT),
                                          strtotime('next weekday') )
        ));

        $fieldset->addField('tipo_promo', 'select', array(
            'label'     => Mage::helper('participe')->__('Type of promotion'),
            'name'      => 'tipo_promo',
            'values'    => array(
                array(
                    'value'     => null,
                    'label'     => Mage::helper('participe')->__('Select type of promotion'),
                ),

                array(
                    'value'     => 1,
                    'label'     => Mage::helper('participe')->__('Text'),
                ),

                array(
                    'value'     => 2,
                    'label'     => Mage::helper('participe')->__('Photo'),
                ),

                array(
                    'value'     => 3,
                    'label'     => Mage::helper('participe')->__('Video'),
                ),
            ),
        ));
        
        $fieldset->addField('tipo_votacao', 'select', array(
            'label'     => Mage::helper('participe')->__('Type of vote'),
            'name'      => 'tipo_votacao',
            'values'    => array(
                array(
                    'value'     => null,
                    'label'     => Mage::helper('participe')->__('Select type of vote'),
                ),

                array(
                    'value'     => 1,
                    'label'     => Mage::helper('participe')->__('Public'),
                ),

                array(
                    'value'     => 2,
                    'label'     => Mage::helper('participe')->__('Private'),
                ),
            ),
        ));

        $fieldset->addField('status_promo', 'select', array(
            'label'     => Mage::helper('participe')->__('Status'),
            'name'      => 'status_promo',
            'value'     => $status,
            'values' => array(
                array(
                    'value'     => 1,
                    'label'     => Mage::helper('participe')->__('Ativa'),
                ),
                array(
                    'value'     => 2,
                    'label'     => Mage::helper('participe')->__('Finalistas'),
                ),
                array(
                    'value'     => 3,
                    'label'     => Mage::helper('participe')->__('Vencedor'),
                ),
            ),
            'disabled' => false,
            'readonly' => false,
            'tabindex' => 1
        ));
        $fieldset->addField('video_promo', 'text', array(
            'label'     => Mage::helper('participe')->__('Video Promotion'),
            'class'     => 'required-entry',
            'required'  => true,
            'name'      => 'video_promo',
        ));

        $fieldset->addField('bg_promo', 'file', array(
            'label'     => Mage::helper('participe')->__('Background Promotion'),
            'name'      => 'bg_promo',
            'onchange' => 'setValueImage(this.value, bg_promo_value);',
            'after_element_html' => '<input type="hidden" id="bg_promo_value" name="bg_promo_value" value="'.$bg_promo.'"/><img style="width: 100%;" src="'.$path.$bg_promo.'" />', 
        ));

        $fieldset->addField('img_promo', 'file', array(
            'label'     => Mage::helper('participe')->__('Imagen Promotion'),
            'name'      => 'img_promo',
            'onchange'  => 'setValueImage(this.value, img_promo_value);',
            'after_element_html' => '<input type="hidden" id="img_promo_value" name="img_promo_value" value="'.$img_promo.'"/><img style="width: 100%;" src="'.$path.$img_promo.'" />',  
        ));
        $scriptBefore = $fieldset->addField('scriptbefore', 'hidden', array(
            'name'      => 'scriptbefore',
        ));
        $scriptBefore->setAfterElementHtml('<script>
        //< ![C
         
            function setValueImage(value, selector){
                var filename = value.split("\\\", 3);
                selector.value = filename[2];
            }
         
        //]]>
        </script>');

        $fieldset->addField('regulamento_promo', 'editor', array(
            'name'      => 'regulamento_promo',
            'label'     => Mage::helper('participe')->__('Terms and conditions'),
            'title'     => Mage::helper('participe')->__('Terms and conditions'),
            'style'     => 'width:98%; height:400px;',
            'wysiwyg'   => false,
            'required'  => true,
        ));
       
        if ( Mage::getSingleton('adminhtml/session')->getParticipeData() )
        {   
            $form->setValues(Mage::getSingleton('adminhtml/session')->getParticipeData());
            Mage::getSingleton('adminhtml/session')->setParticipeData(null);
        } elseif ( Mage::registry('participe_data') ) {
            $form->setValues(Mage::registry('participe_data')->getData());
        }
        return parent::_prepareForm();
    }
}
