<?php
 
	class Innersite_Participe_Block_Adminhtml_Participe extends Mage_Adminhtml_Block_Widget_Grid_Container{
    		public function __construct(){
        		$this->_controller = 'adminhtml_participe';
        		$this->_blockGroup = 'participe';
        		$this->_headerText = Mage::helper('participe')->__('Item Manager');
        		$this->_addButtonLabel = Mage::helper('participe')->__('Add Item');
        		parent::__construct();
    		}
	}
