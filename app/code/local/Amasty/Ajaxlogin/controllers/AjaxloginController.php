<?php

/**
 * @author Amasty Team
 * @copyright Copyright (c) 2013 Amasty (http://www.amasty.com)
 * @package Amasty_Amajaxlogin
 */

require_once 'Mage/Customer/controllers/AccountController.php';

class Amasty_Ajaxlogin_AjaxloginController extends Mage_Customer_AccountController
{
    
    public function preDispatch() {
        
        // a brute-force protection here would be nice
        
        Mage_Core_Controller_Front_Action::preDispatch();
        
        if (!$this->getRequest()->isDispatched()) {
            
            return;
        }
        
        $action = $this->getRequest()->getActionName();
        
        $openActions = array(
        'create', 
        'index', 
        'header', 
        'login', 
        'logoutsuccess', 
        'forgotpassword', 
        'forgotpasswordpost', 
        'resetpassword', 
        'resetpasswordpost', 
        'confirm', 
        'iframe', 
        'confirmation');
        
        $pattern = '/^(' . implode('|', $openActions) . ')/i';
        
        if (!preg_match($pattern, $action)) {
            
            if (!$this->_getSession()->authenticate($this)) {
                
                $this->setFlag('', 'no-dispatch', true);
            }
        } 
        else {
            
            $this->_getSession()->setNoReferer(true);
        }
    }
    
    //show login popup
    
    public function indexAction() {
        $block = Mage::app()->getLayout()->createBlock('amajaxlogin/customer_form_login', 'form_login')->setTemplate('amasty/amajaxlogin/customer/form/login.phtml');
        
        $message = $block->toHtml();
        
        $this->showCartPopup($this->__('Login or Create an Account'), "", $message, 3);
    }
    
    public function forgotpasswordAction() {
        
        $block = Mage::app()->getLayout()->createBlock('amajaxlogin/customer_form_login', 'form_login')->setTemplate('amasty/amajaxlogin/customer/form/forgotpassword.phtml');
        
        $message = $block->toHtml();
        
        $title = $this->__('Forgot Your Password?');
        
        $this->showCartPopup($title, "", $message, 3);
    }
    
    public function forgotpasswordpostAction() {
        
        $block = Mage::app()->getLayout()->createBlock('amajaxlogin/customer_form_login', 'form_login')->setTemplate('amasty/amajaxlogin/customer/form/forgotpassword.phtml');
        
        $message = $block->toHtml();
        
        $title = $this->__('Forgot Your Password?');
        
        $email = (string)$this->getRequest()->getPost('email');
        
        if ($email) {
            
            if (!Zend_Validate::is($email, 'EmailAddress')) {
                
                $this->_getSession()->setForgottenEmail($email);
                
                $this->showCartPopup($title, $this->__('E-mail inválido.'), $message, 1);
                
                return;
            }
            
            /** @var $customer Mage_Customer_Model_Customer */
            
            $customer = Mage::getModel('customer/customer')->setWebsiteId(Mage::app()->getStore()->getWebsiteId())->loadByEmail($email);
            
            if ($customer->getId()) {
                
                try {
                    
                    $newResetPasswordLinkToken = Mage::helper('customer')->generateResetPasswordLinkToken();
                    
                    $customer->changeResetPasswordLinkToken($newResetPasswordLinkToken);
                    
                    $customer->sendPasswordResetConfirmationEmail();
                }
                catch(Exception $exception) {
                    
                    $this->showCartPopup($title, $exception->getMessage(), $message, 1);
                    
                    return;
                }
            }
            
            $this->showCartPopup($title, Mage::helper('customer')->__('Se existe uma conta associada com %s, você receberá um e-mail com um link para redefinir sua senha.', Mage::helper('customer')->htmlEscape($email)), $message, 2);
            
            return;
        } 
        else {
            
            $this->showCartPopup($title, $this->__('Digite seu e-mail.'), $message, 1);
            
            return;
        }
        
        $this->showCartPopup($title, "", $message, 3);
    }
    
    //reload header after login
    
    public function headerAction() {
        
        $this->loadLayout(array('default'));
        
        $header = Mage::app()->getLayout('default')->getBlock('header');
        
        $this->getResponse()->setBody($header->toHtml());
    }
    
    public function logoutAction() {
        $this->_getSession()->logout()->setBeforeAuthUrl(Mage::getUrl());
        
        $this->showCartPopup($this->__('Entrar ou Criar Conta'), $this->__('Agora você está deslogado.'), "", 2);
    }
    
    //default magento login action
    
    public function loginAction() {
        
        
        $title = $this->__('Entrar ou Criar Conta');
        
        $text = "";
        
        $status = 3;
        
        if ($this->_getSession()->isLoggedIn()) {
            
            $text = $this->__('Você está logado ainda.');
            
            $status = 1;
            
            $block = Mage::app()->getLayout()->createBlock('amajaxlogin/customer_form_login', 'form_login')->setTemplate('amasty/amajaxlogin/customer/form/login.phtml');
            
            $message = $block->toHtml();
            
            $this->showCartPopup($title, $text, $message, $status);
            
            return;
        }
        
        $session = $this->_getSession();
        
        if ($this->getRequest()->isPost()) {
            
            $login = $this->getRequest()->getPost('login');
            
            if (!empty($login['username']) && !empty($login['password'])) {
                
                try {
                    
                    $session->login($login['username'], $login['password']);
                    
                    if ($session->getCustomer()->getIsJustConfirmed()) {
                        
                        $text = $this->_welcomeCustomer($session->getCustomer(), true);
                        
                        $status = 2;
                    } 
                    else {
                        
                        $text = $this->__('Agora você está logado.');
                        
                        $status = 2;
                    }
                }
                catch(Mage_Core_Exception $e) {
                    
                    switch ($e->getCode()) {
                        case Mage_Customer_Model_Customer::EXCEPTION_EMAIL_NOT_CONFIRMED:
                            
                            $value = Mage::helper('customer')->getEmailConfirmationUrl($login['username']);
                            
                            $text = Mage::helper('customer')->__('Esta conta não está confirmada. <a href="%s"> Clique aqui </a> para reenviar e-mail de confirmação.', $value);
                            
                            break;

                        case Mage_Customer_Model_Customer::EXCEPTION_INVALID_EMAIL_OR_PASSWORD:
                            
                            $text = $e->getMessage();
                            
                            break;

                        default:
                            
                            $text = $e->getMessage();
                    }
                    
                    $status = 1;
                    
                    $session->setUsername($login['username']);
                }
                catch(Exception $e) {
                    
                    $text = $e->getMessage();
                    
                    $status = 1;
                    
                    $session->setUsername($login['username']);
                    
                    Mage::logException($e);
                     // PA DSS violation: this exception log can disclose customer password
                    
                    
                }
            } 
            else {
                
                $text = $this->__('Faça login e senha são necessários.');
                
                $status = 1;
            }
        }
        
        $block = Mage::app()->getLayout()->createBlock('amajaxlogin/customer_form_login', 'form_login')->setTemplate('amasty/amajaxlogin/customer/form/login.phtml');
        
        $message = $block->tohtml();
        
        $this->showCartPopup($title, $text, $message, $status);
    }
    
    //creating finale popup
    
    public function showCartPopup($title = "", $text = "", $message = "", $is_error = 0) {
        if ($is_error == 1) {
            
            $text = "<div class='am-ajax-error'><img src=" . Mage::getDesign()->getSkinUrl('images/amasty/amajaxlogin/error.png', array('_area' => 'frontend')) . " alt=''/>" . $text . "</div>";
        }
        
        if ($is_error == 2) {
            
            $text = "<div class='am-ajax-success'><img src=" . Mage::getDesign()->getSkinUrl('images/amasty/amajaxlogin/success.png', array('_area' => 'frontend')) . " alt=''/>" . $text . "</div>";
            
            $message = "";
        }
        
        $result = array(
        'title' => $title, 
        'message' => $message, 
        'error' => $text, 
        'is_error' => $is_error);
        
        // get cookie de redirecionamento
        $cookie_redirect = Mage::getModel('core/cookie')->get('redirect_uri');
        
        if (Mage::getStoreConfig('amajaxlogin/general/redirect')) {
            $result['redirect'] = Mage::getStoreConfig('amajaxlogin/general/redirect_url') ? Mage::getStoreConfig('amajaxlogin/general/redirect_url') : Mage::getStoreConfig('amajaxlogin/general/redirect');
        }else if($cookie_redirect){
            $result['redirect'] = $cookie_redirect;
            Mage::getModel('core/cookie')->delete('redirect_uri');    
        }

        $result = $this->replaceJs($result);
        
        $this->getResponse()->setBody($result);
    }
    
    //replace js in one place
    
    public function replaceJs($result) {
        
        $arrScript = array();
        
        $result['script'] = '';
        
        preg_match_all("@<script type=\"text/javascript\">(.*?)</script>@s", $result['message'], $arrScript);
        
        $result['message'] = preg_replace("@<script type=\"text/javascript\">(.*?)</script>@s", '', $result['message']);
        
        foreach ($arrScript[1] as $script) {
            
            $result['script'].= $script;
        }
        
        $result['script'] = preg_replace("@var @s", '', $result['script']);
        
        return Zend_Json::encode($result);
    }
    
    protected function _welcomeCustomer(Mage_Customer_Model_Customer $customer, $isJustConfirmed = false) {
        
        $text = "<p>" . $this->__('Obrigado por registrar com %s.', Mage::app()->getStore()->getFrontendName()) . "</p>";
        
        if ($this->_isVatValidationEnabled()) {
            
            // Show corresponding VAT message to customer
            
            $configAddressType = Mage::helper('customer/address')->getTaxCalculationAddressType();
            
            $userPrompt = '';
            
            switch ($configAddressType) {
                case Mage_Customer_Model_Address_Abstract::TYPE_SHIPPING:
                    
                    $userPrompt = $this->__('Se você é um cliente VAT registado, clique <a href="%s">aqui</a> para inserir o seu endereço de envio para o cálculo correto do VAT', Mage::getUrl('customer/address/edit'));
                    
                    break;

                default:
                    
                    $userPrompt = $this->__('Se você é um cliente VAT registado, clique <a href="%s"> aqui </a> para inserir o seu endereço de faturamento para cálculo adequado VAT', Mage::getUrl('customer/address/edit'));
            }
            
            $this->_getSession()->addSuccess($userPrompt);
        }
        
        $customer->sendNewAccountEmail(
        $isJustConfirmed ? 'confirmed' : 'registered', 
        '', 
        Mage::app()->getStore()->getId());
        
        return $text . "<p>" . $userPrompt . "</p>";
    }
    
    public function _login($userInfo, $token, $type, $typeName) {
        
        $title = 'amajaxlogin_' . $type . '_id';
        
        $customerBySocialId = Mage::helper('amajaxlogin')->getCustomerBySocialId($title, $userInfo['id']);
        
        //add data to the customer
        
        if (Mage::getSingleton('customer/session')->isLoggedIn()) {
            
            if ($customerBySocialId) {
                
                $this->showCartPopup($this->__('Entrar ou Criar Conta'), $this->__('Essa conta já tem outro usuário %s.', $typeName), "", 2);
                
                return;
            }
            
            $customer = Mage::getSingleton('customer/session')->getCustomer();
            
            Mage::helper('amajaxlogin')->connectByEmail(
            $customer, 
            $userInfo['id'], 
            $token, 
            $type);
            
            $this->showCartPopup($this->__('Entrar ou Criar Conta'), $this->__('Você já está logado com sua conta %s', $typeName, $typeName), "", 2);
            
            return;
        }
        
        if ($customerBySocialId) {
            
            // Existing connected user - login
            
            Mage::helper('amajaxlogin')->loginByCustomer($customerBySocialId);
            
            $this->showCartPopup($this->__('Entrar ou Criar Conta'), $this->__('Login realizado com sucesso usando sua conta %s.', $typeName), "", 2);
            
            return;
        }
        
        if ('tw' == $type) {
            
            $this->showCartPopup($this->__('Entrar ou Criar Conta'), $this->__('Nós não encontramos qualquer conta com sua credencial.', $typeName), "", 1);
            
            return;
        }
        
        $customerByEmail = null;
        
        if (array_key_exists('email', $userInfo)) 
        $customerByEmail = Mage::helper('amajaxlogin')->getCustomerByEmail($userInfo['email']);
        
        if ($customerByEmail) {
            
            Mage::helper('amajaxlogin')->connectByEmail(
            $customerByEmail, 
            $userInfo['id'], 
            $token, 
            $type);
            
            $this->showCartPopup($this->__('Entrar ou Criar Conta'), $this->__('Nós descobrimos que você já tem uma conta em nossa loja. Conta Sua %s está agora ligado à sua conta da loja.', $typeName), "", 2);
            
            return;
        }
        
        // New connection - create, attach, login
        
        if (empty($userInfo['first_name'])) {
            
            $this->showCartPopup($this->__('Entrar ou Criar Conta'), $this->__('Desculpe, não pôde recuperar a sua %s primeiro nome. Por favor, tente novamente.', $typeName), "", 1);
            
            return;
        }
        
        if (empty($userInfo['last_name'])) {
            
            $this->showCartPopup($this->__('Entrar ou Criar Conta'), $this->__('Desculpe, não pôde recuperar a sua %s sobrenome. Por favor, tente novamente.', $typeName), "", 1);
            
            return;
        }
        
        if (empty($userInfo['email'])) {
            
            $this->showCartPopup($this->__('Entrar ou Criar Conta'), $this->__('Desculpe, não pôde recuperar a sua %s e-mail. Por favor, tente novamente.', $typeName), "", 1);
            
            return;
        }
        
        Mage::helper('amajaxlogin')->connectByCreatingAccount(
        $userInfo['email'], 
        $userInfo['first_name'], 
        $userInfo['last_name'], 
        $userInfo['id'], 
        $token, 
        $type);
        
        $this->showCartPopup($this->__('Entrar ou Criar Conta'), $this->__('Você já está logado com sua conta %s.', $typeName, $typeName), "", 2);
    }
}

