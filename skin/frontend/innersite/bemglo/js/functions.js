var func = (function(){
  "use strict"
  
  var initiStrech = function(uri){
    jQuery('#header').backstretch(uri);
  };

  var initCountList = function(){
    var counters = jQuery(".count-promo");
    if(counters.length){
      jQuery.each(counters, function(){
        var countthis = new Countdown({
          selector: "#"+jQuery(this)[0].id,
          dateEnd: new Date(jQuery(this).attr("data-vigencia")),
          msgPattern : '<div class="wrapcount">{days}<span class="legend">Dias</span></div><span class="pontos">:</span><div class="wrapcount">{hours}<span class="legend">Horas</span></div><span class="pontos">:</span><div class="wrapcount">{minutes}<span class="legend">Minutos</span></div><span class="pontos">:</span><div class="wrapcount">{seconds}<span class="legend">segundos</span></div>',
        });
      });
    }
  };

  var singleCount = function(){
    new Countdown({
      selector: "#count-down-main-promo",
      dateEnd: new Date(jQuery("#count-down-main-promo").attr("data-vigencia")),
      msgPattern : '<div class="wrapcount">{days}<span class="legend">Dias</span></div><span class="pontos">:</span><div class="wrapcount">{hours}<span class="legend">Horas</span></div><span class="pontos">:</span><div class="wrapcount">{minutes}<span class="legend">Minutos</span></div><span class="pontos">:</span><div class="wrapcount">{seconds}<span class="legend">segundos</span></div>',
    });
  }

  return {
    init: function(uriBacktrecht){
      initiStrech(uriBacktrecht);
    },
    listPromos: function(){
      initCountList()
    },
    single: function(){
      singleCount();
    } 
  };

})(jQuery);